package com.inditex.pricequeryservice;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.junit.jupiter.api.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.is;
import java.math.BigDecimal;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class PriceQueryServiceApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	private final String urlTemplate = "/api/v1/prices?date={date}&productId={productId}&brandId={brandId}";

	@Test
	public void whenRequestAt10On14thForProduct35455AndBrand1_thenReturnCorrectPrice() throws Exception {
		mockMvc.perform(get(urlTemplate, "2020-06-14-10.00.00", 35455, 1)
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.productId", is(35455)))
				.andExpect(jsonPath("$.brandId", is(1)))
				.andExpect(jsonPath("$.price", is(new BigDecimal("35.50").doubleValue())));
	}


	@Test
	public void whenRequestAt16On14thForProduct35455AndBrand1_thenReturnCorrectPrice() throws Exception {
		mockMvc.perform(get(urlTemplate, "2020-06-14-16.00.00", 35455, 1)
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.productId", is(35455)))
				.andExpect(jsonPath("$.brandId", is(1)))
				.andExpect(jsonPath("$.price", is(new BigDecimal("25.45").doubleValue())));
	}

	@Test
	public void whenRequestAt21On14thForProduct35455AndBrand1_thenReturnCorrectPrice() throws Exception {
		mockMvc.perform(get(urlTemplate, "2020-06-14-21.00.00", 35455, 1)
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.productId", is(35455)))
				.andExpect(jsonPath("$.brandId", is(1)))
				.andExpect(jsonPath("$.price", is(new BigDecimal("35.50").doubleValue())));
	}

	@Test
	public void whenRequestAt10On15thForProduct35455AndBrand1_thenReturnCorrectPrice() throws Exception {
		mockMvc.perform(get(urlTemplate, "2020-06-15-10.00.00", 35455, 1)
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.productId", is(35455)))
				.andExpect(jsonPath("$.brandId", is(1)))
				.andExpect(jsonPath("$.price", is(new BigDecimal("30.50").doubleValue())));
	}

	@Test
	public void whenRequestAt21On16thForProduct35455AndBrand1_thenReturnCorrectPrice() throws Exception {
		mockMvc.perform(get(urlTemplate, "2020-06-16-21.00.00", 35455, 1)
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.productId", is(35455)))
				.andExpect(jsonPath("$.brandId", is(1)))
				.andExpect(jsonPath("$.price", is(new BigDecimal("38.95").doubleValue())));
	}
}
