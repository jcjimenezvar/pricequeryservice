package com.inditex.pricequeryservice.service;

import com.inditex.pricequeryservice.dto.PriceResponseDto;
import com.inditex.pricequeryservice.model.Price;
import com.inditex.pricequeryservice.repository.PriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PriceService {

    @Autowired
    private PriceRepository priceRepository;

    public List<PriceResponseDto> findPrices(LocalDateTime date, int productId, int brandId) {
        List<Price> prices = priceRepository.findApplicablePrices(date, productId, brandId);
        return prices.stream().map(this::mapToDto).collect(Collectors.toList());
    }

    private PriceResponseDto mapToDto(Price price) {
        return new PriceResponseDto(price.getProductId(),
                price.getBrandId(),
                price.getPriceList(),
                price.getStartDate(),
                price.getEndDate(),
                price.getPrice());
    }
}
