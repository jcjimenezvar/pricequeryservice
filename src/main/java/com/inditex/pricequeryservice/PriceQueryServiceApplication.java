package com.inditex.pricequeryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PriceQueryServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(PriceQueryServiceApplication.class, args);
    }
}
