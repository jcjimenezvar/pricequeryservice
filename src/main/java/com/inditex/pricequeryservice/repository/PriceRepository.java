package com.inditex.pricequeryservice.repository;

import com.inditex.pricequeryservice.model.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Repository
public interface PriceRepository extends JpaRepository<Price, Long> {
    @Query("SELECT p FROM Price p WHERE :date BETWEEN p.startDate AND p.endDate " +
            "AND p.productId = :productId AND p.brandId = :brandId " +
            "ORDER BY p.priority DESC")
    List<Price> findApplicablePrices(LocalDateTime date, int productId, int brandId);
}
