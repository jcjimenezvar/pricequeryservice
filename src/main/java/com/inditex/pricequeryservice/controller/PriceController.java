package com.inditex.pricequeryservice.controller;

import com.inditex.pricequeryservice.dto.PriceResponseDto;
import com.inditex.pricequeryservice.service.PriceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/api/v1")
public class PriceController {

    @Autowired
    private PriceService priceService;

    @Operation(summary = "Find applicable price",
            description = "Retrieves the applicable price for a given product and brand at a specific date and time.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful retrieval of the price",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PriceResponseDto.class))}),
            @ApiResponse(responseCode = "404", description = "Price not found for the specified parameters",
                    content = @Content)
    })
    @GetMapping("prices")
    public ResponseEntity<PriceResponseDto> findPrices(
            @RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd-HH.mm.ss") LocalDateTime date,
            @RequestParam("productId") Integer productId,
            @RequestParam("brandId") Integer brandId) {
        List<PriceResponseDto> priceResponse = priceService.findPrices(date, productId, brandId);
        if (priceResponse.isEmpty()) {
            // No prices found for the given parameters
            return ResponseEntity.notFound().build();
        }

        PriceResponseDto applicablePrice = priceResponse.get(0);
        return ResponseEntity.ok(applicablePrice);
    }

    @GetMapping
    public String welcome(Model model) {
        return "welcome";  // Name of the Thymeleaf template (without .html extension)
    }
}
