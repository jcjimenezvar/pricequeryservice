# Build stage with Maven and an available JDK version
FROM maven:3.8.4-openjdk-17 as build

# Set the working directory in the Docker image
WORKDIR /app

# Copy the Maven configuration file and checkstyle configuration
COPY pom.xml .
COPY checkstyle.xml /app/checkstyle.xml

# Download all required dependencies into one layer
RUN mvn dependency:go-offline -B

# Copy the project source code
COPY src src

# Run Checkstyle, tests, and package the application
RUN mvn checkstyle:check -Dcheckstyle.config.location=/app/checkstyle.xml && mvn test && mvn package

# Final stage with OpenJDK to run the application
FROM openjdk:17

# Copy the JAR file from the build stage to the final image
COPY --from=build /app/target/*.jar /app/app.jar

# Expose the port the application runs on
EXPOSE 3000

# Command to run the application
CMD ["java", "-jar", "/app/app.jar"]
