
# Price Query Service

## Description

Price Query Service is a Spring Boot application designed to provide an API for querying product prices based on various criteria such as product ID, brand, and date. This service is ideal for e-commerce platforms within the Inditex group, aiming to offer dynamic pricing information.

## Features

- REST API to query product prices.
- Integration with H2 in-memory database for quick setup and testing.
- Docker support for easy deployment and scaling.
- Maven-based build system for easy dependency management.

## Prerequisites

- JDK 17
- Maven 3.8+
- Docker (optional for container deployment)

## Local Setup

1. **Clone the Repository**

    ```bash
    git clone https://gitlab.com/jcjimenezvar/pricequeryservice.git
    cd pricequeryservice
    ```

2. **Build the Application**

   Use Maven to compile the application:

    ```bash
    mvn clean install
    ```

3. **Run the Application**

   Start the Spring Boot application with:

    ```bash
    mvn spring-boot:run
    ```
   or
    ```bash
    ./mvnw spring-boot:run
    ```
   The service will start on `http://localhost:3000/api/v1`. [Go to Welcome Page](http://localhost:3000/api/v1)

4. **Test the Application**

   Test the Spring Boot application with:

    ```bash
    mvn test
    ```
   or
    ```bash
    ./mvnw test
    ```

## Docker Deployment

1. **Build Docker Image**

    ```bash
    docker build --no-cache -t pricequeryservice .
    ```

2. **Run the Container**

    ```bash
    docker stop price-query-service && docker rm price-query-service && docker run --name price-query-service -p 3000:3000 pricequeryservice
   ```
   The service will start on `http://localhost:3000/api/v1`. [Go to Welcome Page](http://localhost:3000/api/v1)

## API Usage

- **Query Prices**: `GET /api/prices?date={date}&productId={productId}&brandId={brandId}`

  Replace `{date}`, `{productId}`, and `{brandId}` with your query parameters.

  [See more details and test](http://localhost:3000/swagger-ui/index.html)

## Models

-   **Price**: Represents a price with properties `id`, `brandId`, `startDate`, `endDate`, `priceList`, `productId`, `priority`, `price` and `curr`.

## H2 Database

To access the H2 Database Console while the application is running inside Docker:

1. Ensure that `application.properties` includes the following settings to enable the H2 console and allow remote connections:
   ```properties
   spring.h2.console.enabled=true
   spring.h2.console.settings.web-allow-others=true
   spring.datasource.url=jdbc:h2:mem:inditex
   spring.datasource.driverClassName=org.h2.Driver
   spring.datasource.username=admin
   spring.datasource.password=admin

2. After starting the Docker container, navigate to: [See database ](http://localhost:3000/h2-console)
3. Log in with the appropriate JDBC URL, username, and password as configured for your H2 database.

Note: Allowing remote connections to the H2 console can pose a security risk. It's recommended to use this only in a development environment.

## Configuration

The application can be configured via the `application.properties` file, which includes database settings, server port, and other Spring Boot configurations. Adjust these settings according to your environment.

## Contributing

Contributions are welcome! Please fork the repository and submit a pull request with your proposed changes.
